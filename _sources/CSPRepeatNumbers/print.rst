..  Copyright (C)  Mark Guzdial, Barbara Ericson, Briana Morrison
    Permission is granted to copy, distribute and/or modify this document
    under the terms of the GNU Free Documentation License, Version 1.3 or
    any later version published by the Free Software Foundation; with
    Invariant Sections being Forward, Prefaces, and Contributor List,
    no Front-Cover Texts, and no Back-Cover Texts.  A copy of the license
    is included in the section entitled "GNU Free Documentation License".
    
.. qnum::
    :start: 1
    :prefix: csp-7-6-
	
.. highlight:: python 
    :linenothreshold: 4
   
Adding Print Statements
=======================

The goal of this stage of learning about programming is to develop a mental
model of how the program works.  Can you look at a program and *predict* what's
going to happen?  Can you figure out the values of the variables?  Feel free to
insert lots of ``print()`` function calls.  Make a prediction about variable
values, then insert ``print()`` calls to display the variable values, and run
the program to find out whether the prediction is right.  Run this version to
see what gets printed.

.. activecode:: Numbers_Sum_Print
    :tour_1: "Code tour"; 2: accL/line2; 5: accL/line5; 6: accL/line6; 9: accL/line9; 10: accL/line10; 12: accL/line12; 15: accL/line15;

    # Step 1: Initialize accumulator 
    sum = 0  # Start out with nothing

    # Step 2: Get data
    numbers = range(0, 101, 2)
    print("All the numbers:", numbers)

    # Step 3: Loop through the data
    for number in numbers:
    	print("Number:", number)
    	# Step 4: Accumulate
    	sum = sum + number

    # Step 5: Process final result
    print(sum)
    
.. parsonsprob:: 7_6_1_Print-Sum-Evens

   <p>The following is the correct code for printing the value of number and
   the sum each time through the loop, but it is mixed up. The code should
   initialize the accumulator, create the list of numbers, and then loop
   through the list of numbers. Each time through the loop it should print the
   value of number, add the value of number to the accumulator, and then print
   the <i>current sum</i>. Note that this is different from the previous
   example, where only the <i>final sum</i> was printed.</p>
   
   <p>Drag the blocks from the left and put them in the correct order on the
   right.  Don't forget to indent blocks in the body of the loop.  Just drag
   the block further right to indent.  Click the <i>Check Me</i> button to
   check your solution.</p>
   -----
   sum = 0  
   =====
   numbers = range(0, 101, 2)
   =====
   for number in numbers:
   =====
       print("Number:", number)
   =====
       sum = sum + number
   =====
       print(sum)
