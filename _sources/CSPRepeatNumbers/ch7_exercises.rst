..  Copyright (C)  Brad Miller, David Ranum, Jeff Elkner, Peter Wentworth,
    Allen B. Downey, Chris Meyers, and Dario Mitchell.  Permission is granted
    to copy, distribute and/or modify this document under the terms of the GNU
    Free Documentation License, Version 1.3 or any later version published by
    the Free Software Foundation; with Invariant Sections being Forward,
    Prefaces, and Contributor List, no Front-Cover Texts, and no Back-Cover
    Texts.  A copy of the license is included in the section entitled "GNU Free
    Documentation License".


.. setup for automatic question numbering.

.. qnum::
    :start: 1
    :prefix: 7-8-

Chapter 7 Exercises
--------------------

#.
    .. tabbed:: ch7ex1t

        .. tab:: Question

            Complete lines 1 and 2 below to create the code to add up all the
            numbers from 1 to 5 and print the sum.

            .. activecode:: ch7ex1q
                :nocodelens:

                sum = ??
                things_to_add = ??
                for number in things_to_add:
                    sum = sum + number
                print(sum)

#.
    .. tabbed:: ch7ex2t

        .. tab:: Question

            Fix the errors in the code so that it prints the sum of all the
            numbers 1 to 10.

            .. activecode::  ch7ex2q
                :nocodelens:

                sum = 0
                num_list = range(1,10)
                for number in num_list:
                    sum = number
                print(sum)

#.
    .. tabbed:: ch7ex3t

        .. tab:: Question

           Change the code below into a function that returns the sum of a list
           of numbers.  Pass the list of numbers as a parameter and print the
           result of calling the function.

           .. activecode::  ch7ex3q
                :nocodelens:

                def sum_list(numlist): 
                    sum = ??                 # Start with additive identity 
                    things_to_add = ??       # Set things_to_add to parameter
                    for n in things_to_add:  # Loop through each number
                        ?? = ?? + ??         # Accumlate the sum
                    return ?? 

                print(sum_list(??))          # Print the call result to test

#.
    .. tabbed:: ch7ex4t

        .. tab:: Question

           Fill in the missing code on lines 3 and 4 to loop through the list
           of numbers and calculate the *product*.  Add a line at the end to
           print the value in ``product``.

           .. activecode::  ch7ex5q
                :nocodelens:

                product = 1      # Start with the multiplicative identity 
                numbers = [1, 2, 3, 4, 5]
                for ?? in numbers:
                    product = product * ??
                ??               # Print the result

#.
    .. tabbed:: ch7ex5t

        .. tab:: Question

            Fix the errors in the code so that it prints the product of every
            5th number between 5 and 25, inclusive. So, the product of 5, 10,
            15, ..., 25. It should print the value ``375000`` if you have done
            everything correctly.

            .. activecode::  ch7ex4q
                :nocodelens:

                product = 0
                numbers = len(4, 25, 5)
                for number in numbers:
                    product = product + number
                print(product)


#.
    .. tabbed:: ch7ex6t

        .. tab:: Question

            Fix the errors in the code so that it prints the sum of all the odd
            numbers 1 through 20.

            .. activecode::  ch7ex6q
                :nocodelens:

                sum = 1
                numbers = range(1, 21, 1)
                for numbers in number:
                sum = sum + number
                print(sum)

#.
    .. tabbed:: ch7ex7t

        .. tab:: Question

           Modify the code below to create a function that calculates the
           product of a list of numbers and returns it. Have the function take
           a list of numbers as a parameter.  Call the function to test it and
           print the result of calling the function.

           .. activecode::  ch7ex7q
                :nocodelens:

                product = 1  # Start out with 1
                numbers = [1, 2, 3, 4, 5]
                for number in numbers:
                    product = product * number
                print(product)

#.
    .. tabbed:: ch7ex8t

        .. tab:: Question

            Fix the error in the code so that it takes each string in the list
            and prints out the sentence "I like to eat pizza".

            .. activecode::  ch7ex8q
                :nocodelens:

                a_string = ""
                a_list = ["I", "like", "to", "eat", "pizza"]
                for word in a_list:
                    a_string = word
                    print(a_string)

#.
    .. tabbed:: ch7ex9t

        .. tab:: Question

           Fill in the code below on lines 2, 5, and 10 to correctly add up and
           print the sum of all the even numbers from 0 to 10 (inclusive).

           .. activecode::  ch7ex9q
                :nocodelens:

                # Step 1: Initialize accumulator
                sum = ??   # Start with nothing (addition identity value) 

                # Step 2: Get data
                numbers = range(??)

                # Step 3: Loop through the data
                for number in numbers:
                    # Step 4: Accumulate
                    sum = sum + ??
                # Step 5: Display result
                print(sum)

#.
    .. tabbed:: ch7ex10t

        .. tab:: Question

            Write code that prints the square of each number 1 through 10 in
            the format "1 * 1 = 1", etc.

            .. activecode::  ch7ex10q
                :nocodelens:

#.
    .. tabbed:: ch7ex11t

        .. tab:: Question

            The code below sums the even numbers from ``0`` to ``20``
            inclusive.  Turn it into a function ``sum_evens(to_num)`` that
            calculates the sum of the even numbers from ``0`` to the value of
            ``to_num`` passed to the function. Return the sum from the
            function. Call the function inside ``print`` to see the result.

            .. activecode::  ch7ex11q
               :nocodelens:

                 # Step 1: Initialize accumulator
                 sum = 0  # Start out with nothing

                 # Step 2: Get data
                 numbers = range(0, 21, 2)

                 # Step 3: Loop through the data
                 for number in numbers:
                     # Step 4: Accumulate
                     sum = sum + number

                 # Step 5: Process result
                 print(sum)

#.
    .. tabbed:: ch7ex12t

        .. tab:: Question

            The `factorial <https://en.wikipedia.org/wiki/Factorial>`__ of a
            positive integer ``n`` is the product of all the integers from
            ``n`` down to ``1``.

            Complete the function below that returns the factorial of an
            integer passed to it and print the result of calling the function.

            .. activecode::  ch7ex12q
                :nocodelens:

                def factorial(n):
                    fact = ??                      # Set fact to passed value 
                    for i in range(??, ??, -1):    # Count down from n to 1
                        fact = ?? * ??             # Accumulate the product
                    return ??

                ??                                 # Call and print to test

#.
    .. tabbed:: ch7ex13t

        .. tab:: Question

            Fix the code below to correctly calculate and return the product of
            all of the even numbers from 10 to 20.

            .. activecode::  ch7ex13q
                 :nocodelens:

                 # Step 1: Initialize accumulator
                 product = 0  # initialize product to multiplicative identity

                 # Step 2: Get data
                 numbers = range(10, 20, 2)

                 # Step 3: Loop through the data
                 for number in numbers:
                     # Step 4: Accumulate
                     product = product + number

                 # Step 5: Process result
                 print(product)

#.
    .. tabbed:: ch7ex14t

        .. tab:: Question

            Use your solution to the previous exerice to complete the function
            below to calculate and return the sum of all of the even numbers
            from ``from_num`` to ``to_num`` inclusive. Calling ``sum_evens(6,
            10)`` should return ``24``. Print the result of calling the
            function to test it.

            .. activecode::  ch7ex14q
                 :nocodelens:

                def sum_evens(from_num, to_num):
                    # Step 1: Initialize accumulator      


                    # Step 2: Get data
                    numbers = range(??, ??, 2)

                    # Step 3: Loop through the data
                    for number in numbers:
                        # Step 4: Accumulate
                        product = product * number

                    # Step 5: Return result
                    return ?? 

                # Step 6: Print the result of calling the function
                print(??)


#.
    .. tabbed:: ch7ex15t

        .. tab:: Question

            Create a list of all odd numbers from 1 to 20 and find the average.
            Then create a list of numbers from 1 to 100 using the average as
            the increment and print the product of those numbers.

            .. activecode::  ch7ex15q
                :nocodelens:

#.
    .. tabbed:: ch7ex16t

        .. tab:: Question

           Create a procedure to calculate and return the sum of all of the odd
           numbers from 1 to a passed last number (inclusive).  Call the
           function to test and it print the result.

           .. activecode::  ch7ex16q
                :nocodelens:

#.
    .. tabbed:: ch7ex17t

        .. tab:: Question

            Complete the code for a function that takes a list of letters and
            combines them into a word. It should print "Hi".

            .. activecode::  ch7ex17q
                :nocodelens:

                def letter_combiner(??):
                    temp_string = ??
                    for ?? in letter_list:
                        temp_string = temp_string + letter
                    return ??

                a_list = ["H", "i"]
                print(letter_combiner(??))

#.
    .. tabbed:: ch7ex18t

        .. tab:: Question

           Create a function to calculate and return the product of all of the
           even numbers from 2 to the passed end number (inclusive).  Be sure
           to call the function to test it and print the result.

           .. activecode::  ch7ex18q
                :nocodelens:

#.
    .. tabbed:: ch7ex19t

        .. tab:: Question

            Write a function that takes two inputs, a start and stop for a
            range (inclusive). Find the product and the sum of all the numbers
            and return the average between those two numbers. make a call to
            the function where you print the result

            .. activecode::  ch7ex19q
                :nocodelens:

#.
    .. tabbed:: ch7ex20t

        .. tab:: Question

           Write a function that will take a list of numbers and return the
           average.  Remember that the average is the sum of all of the numbers
           in the list divided by the number of items in the list.  You can get
           the length of a list using the ``len(list)`` function.

           .. activecode::  ch7ex20q
               :nocodelens:

#.
    .. tabbed:: ch7ex21t

        .. tab:: Question

            Create a function that takes one integer parameter and gets a list
            of all the odd numbers in that range and all the even numbers in
            that range. Find the product of all the even numbers, the sum of
            all the odd numbers, and then return the difference of the product
            by the sum and divide by the average of the two. Call the function
            and print the result.

            .. activecode::  ch7ex21q
                :nocodelens:
