..  Copyright (C)  Brad Miller, David Ranum, Jeffrey Elkner, Peter Wentworth,
    Allen B. Downey, Chris Meyers, and Dario Mitchell.  Permission is granted
    to copy, distribute and/or modify this document under the terms of the GNU
    Free Documentation License, Version 1.3 or any later version published by
    the Free Software Foundation; with Invariant Sections being Forward,
    Prefaces, and Contributor List, no Front-Cover Texts, and no Back-Cover
    Texts.  A copy of the license is included in the section entitled "GNU Free
    Documentation License".


.. setup for automatic question numbering.

.. 	qnum::
	:start: 1
	:prefix: 16-12-

Chapter 16 Exercises
---------------------

#.
    .. tabbed:: ch16ex1t

        .. tab:: Question

            Fix syntax 6 errors in the code below so that the code runs correctly. It should set ``combined`` to the concatenation of ``start`` and ``name``.  It should print the length of the combined string, print the combined string, and it should print the result of ``name * 3``.

            .. activecode:: ch16ex1q
                :nocodelens:

                name = 'Mark"
                start = "My name is '
                combined = start  name
                print(len(combined)
                print(combined)
                print name * 3

#.

    .. tabbed:: ch16ex2t

        .. tab:: Question

            Add code to line 1 so that the code below prints 14.

            .. activecode::  ch16ex2q
                :nocodelens:

                a_string =
                b_string = "bow"
                c_string = (a_string + b_string) * 2
                print(len(c_string))

#.

    .. tabbed:: ch16ex3t

        .. tab:: Question

           Fix the 5 syntax errors in the code below so that it runs.  It should print the length of ``my_first_list`` and print the result of ``my_first_list * 3``.  Then it should set ``my_second_list`` to the concatenation of ``my_first_list`` and a list containing ``321.4``.  Then it should print the value of ``my_second_list``.

           .. activecode::  ch16ex3q
                :nocodelens:

                my_first_list = [12,"ape"13]
                print(len(my_first_list)
                print(my_firstlist * 3)
                my_second_list = my_first_list + [321.4
                print(my_second_list

#.

    .. tabbed:: ch16ex4t

        .. tab:: Question

            Fix the errors so that the procedure prints "My name is JohnJohn" and "19".

            .. activecode::  ch16ex4q
                :nocodelens:

                def name_procedure(name):
                start = "My name is"
                combined = start * (name * 2)
                print(combined)
                print(len(combined)


                name_procedure(John)

#.

    .. tabbed:: ch16ex5t

        .. tab:: Question

           Fix 5 syntax errors in the code below so that it runs and prints the contents of ``items``.

           .. activecode::  ch16ex5q
                :nocodelens:

               def item_lister(items):
                   items[0] = "First item'
                   items[1] = items0]
                   items[2] = items[2] + 1
                   print items

                item_lister([2, 4, 6 8])

#.

    .. tabbed:: ch16ex6t

        .. tab:: Question

            Complete the code on lines 4 and 5 so that the function returns the average of a list of integers.

            .. activecode::  ch16ex6q
                :nocodelens:

                def grade_average(a_list):
                    sum = 0
                    for num in a_list:

                    average =
                    return average

                a_list = [99, 100, 74, 63, 100, 100]
                print(grade_average(a_list))

#.

    .. tabbed:: ch16ex7t

        .. tab:: Question

           Fix the indention in the code below so that it runs correctly.  It should loop and add the current value of ``source`` to ``so_far`` each time through the loop.  It should also print the value of ``so_far`` each time through the loop.

           .. activecode::  ch16ex7q
                :nocodelens:

                source = ["This", "is", "a", "list"]
                so_far = []
                for index in range(0,len(source)):
                so_far = [source[index]] + so_far
                print(so_far)

#.
    .. tabbed:: ch16ex8t

        .. tab:: Question

            Fix the code so that the code prints "['hihi', 0, 0, 4]" .

            .. activecode::  ch16ex8q
                :nocodelens:

                items = ["hi" 2, 3, 4]
                items[0] = items[0] * items0
                items(1) = items[2] - 3
                items[2] = items[1]
                print(items)

#.
    .. tabbed:: ch16ex9t

        .. tab:: Question

           Fix 4 syntax errors in the code below.  After the code executes the list ``so_far`` should contain the reverse of the ``source`` list.

           .. activecode::  ch16ex9q
                :nocodelens:

                # setup the source list
                source = ["This","is" "a","list"]

                # Set the accumulator to the empty list
                so_far = [

                # Loop through all the items in the source list
                for index in range(0,len(source))

                    # Add the current item in the source and print the current items in so_far
                    so_far = [source[index]] + sofar
                    print(so_far)

#.
    .. tabbed:: ch16ex10t

        .. tab:: Question

            The code below currently prints the reverse of a list. Change it so that it prints a mirrored version of the list. It should print "['list', 'a', 'is', 'This', 'This', 'is', 'a', 'list']".

            .. activecode::  ch16ex10q
                :nocodelens:

                # setup the source list
                source = ["This","is","a","list"]

                # Set the accumulator to the empty list
                so_far = []

                # Loop through all the items in the source list
                for index in range(0,len(source)):

                    # Add a list with the current item from source to so_far
                    so_far =  [source[index]] + so_far
                print(so_far)

#.

    .. tabbed:: ch16ex11t

        .. tab:: Question

           Change the following code into a function.  It should take the list and return a list of the values at the even indicies.

           .. activecode::  ch16ex11q
                :nocodelens:

                numbers = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
                even_list = []
                for index in range(0,len(numbers),2):
                    even_list = even_list + [numbers[index]]
                print(even_list)

#.
    .. tabbed:: ch16ex12t

        .. tab:: Question

            The following code creates and prints a list of even numbers. Change it and add to it so that it creates a list of all multiples of 5 from 0 to 50, inclusive.

            .. activecode::  ch16ex12q
                :nocodelens:

                # initialize the variables
                numbers = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
                evens = []

                # loop though every other index
                for index in range(0,len(numbers),2):

                    # add the lists
                    evens = evens + [numbers[index]]

                # print the result
                print(evens)

#.

    .. tabbed:: ch16ex13t

        .. tab:: Question

           Change the following into a procedure. It prints a countdown from 5 to 0.  Have it take the starting number for the countdown as a parameter.  Print each value till it gets to 0.

           .. activecode::  ch16ex13q
                :nocodelens:

                for index in range(5, -1, -1):
                    print(index)

#.

    .. tabbed:: ch16ex14t

        .. tab:: Question

            Fix the errors so that the code individually adds each item from ``source`` to ``new_list``. Make the range decrement, so it starts from the end, but keep ``new_list`` in the same order as ``source``.

            .. activecode::  ch16ex14q
                :nocodelens:

                # initialize the variables
                source = ["This", "is", "a", "list"]
                new_list = []

                # loop from the last index to the first (0)
                for index in range(len(source), 1, -1):

                # append the lists
                new_list = new_list + [source[index]]

                # print the current value of the list
                print(new_list)

#.
    .. tabbed:: ch16ex15t

        .. tab:: Question

           Write a function that returns the values at the odd indices in a list.  The function should take the number list as a parameter.  If it is passed [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10] for example, it should return [1, 3, 5, 7, 9].

           .. activecode::  ch16ex15q
                :nocodelens:

#.
    .. tabbed:: ch16ex16t

        .. tab:: Question

            Write a function that takes a list of numbers as a parameter and adds 5 to each number and returns the list.

            .. activecode::  ch16ex16q
                :nocodelens:

#.
    .. tabbed:: ch16ex17t

        .. tab:: Question

           Write a function that takes a list of numbers and returns the sum of the positive numbers in the list.

           .. activecode::  ch16ex17q
                :nocodelens:

#.
    .. tabbed:: ch16ex18t

        .. tab:: Question

            Write a function that takes in a list of numbers as a parameter. The function should calculate the sum of all the positive numbers in the list, the absolute value of the sum of the negative numbers, and return the average of the two sums.

            .. activecode::  ch16ex18q
                :nocodelens:

#.
    .. tabbed:: ch16ex19t

        .. tab:: Question

           Write a function to return the reverse of a list, but with only every other item from the original list starting at the end of the list.  So, if it is passed the list [0,1,2,3,4,5] for example, it should return the list [5, 3, 1].

           .. activecode::  ch16ex19q
               :nocodelens:

#.
    .. tabbed:: ch16ex20t

        .. tab:: Question

            Write a procedure that takes an int as a parameter. The procedure should add every other odd number from 1 to the int parameter (inclusive) into a new list. The procedure should print the new list and the sum of the new list.

            .. activecode::  ch16ex20q
                :nocodelens:
